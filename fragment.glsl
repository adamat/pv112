#version 330

out vec4 final_color;

in vec3 VS_normal_ws;
in vec3 VS_position_ws;
in vec2 VS_tex_coord;
in vec3 objpos;

uniform float general_alpha;
uniform mat3 normal_matrix;

const int n_lights = 2;
uniform vec4 light_position[n_lights];
uniform vec3 light_ambient_color;
uniform vec3 light_diffuse_color[n_lights];
uniform vec3 light_specular_color[n_lights];
uniform int n_spots;
uniform vec4 spotlight_position[4];
uniform vec3 eye_position;
uniform int piece_number;

uniform sampler2D diffuse_tex;
uniform sampler2D normal_tex;

vec3 material_specular_color;
float material_shininess;

//
// Description : Array and textureless GLSL 3D simplex noise functions.
//      Author : Ian McEwan, Ashima Arts.
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License.
//               https://github.com/ashima/webgl-noise
// 

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x) {
     return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise3(vec3 v)
{ 
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

  // First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

  // Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //   x0 = x0 - 0.0 + 0.0 * C.xxx;
  //   x1 = x0 - i1  + 1.0 * C.xxx;
  //   x2 = x0 - i2  + 2.0 * C.xxx;
  //   x3 = x0 - 1.0 + 3.0 * C.xxx;
  vec3 x1 = x0 - i1 + C.xxx;
  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
  vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

  // Permutations
  i = mod289(i); 
  vec4 p = permute( permute( permute( 
	  i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
	+ i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
      + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

  // Gradients: 7x7 points over a square, mapped onto an octahedron.
  // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
  float n_ = 0.142857142857; // 1.0/7.0
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

  //Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

  // Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
	dot(p2,x2), dot(p3,x3) ) );
}

vec3 procedural_wood(vec3 pos)
{
  float realy = pos.y;
  pos.y *= 0.2;
  pos.y += piece_number * 4;
  vec2 ce = vec2(piece_number / 4 - 0.3 * (piece_number % 3), piece_number % 4 -
  1.2 * (piece_number % 5)) * 0.3;
  float tu = snoise3(pos) + 0.5 * snoise3(pos*2.0)/*+ 0.2*snoise3(pos*4.0)*/;
  float letokruh = abs(sin(1.0 * tu + 10.0 * sqrt(pow(pos.x - ce.x, 2) +
  pow(pos.z - ce.y, 2))));
  vec3 color;
  if (piece_number < 8) {
    color = mix(vec3(0.54, 0.43, 0.28), vec3(0.75, 0.65, 0.45), letokruh);
    material_shininess = 5.0;
    material_specular_color = vec3(0.1, 0.1, 0.1);
  } else {
    color = mix(vec3(0.2, 0.15, 0.06), vec3(0.3, 0.2, 0.02), letokruh);
    material_shininess = 10.0;
    material_specular_color = vec3(0.25, 0.25, 0.25);
  }
  if (piece_number % 2 == 1) {
    float r = sqrt(pow(pos.x, 2) + pow(pos.z, 2));
    color += vec3(0.4, 0.1, 0.0) * smoothstep(0.345, 0.30, r);
  }
  color -= 0.13 * smoothstep(0.15, 0.0, realy);
  return color;
}

vec3 procedural_polkagris(vec3 oPos)
{
  vec3 col, c1, c2;
  if (piece_number < 8)
    c1 = vec3(0.7, 0.1, 0.8);
  else
    c1 = vec3(1.0, 0.1, 0.1);
  c2 = vec3(0.95, 0.95, 0.95);

  float a = 1.5 * oPos.y;
  vec3 mPos = mat3(cos(a), 0.0, sin(a), 0.0, 0.0, 0.0, -sin(a), 0.0, cos(a)) * oPos;
  float rt = abs((mPos.x) / (mPos.z));
  //if (rt < tan(0.25) || rt  > tan(1.3))
  col = mix(c2, c1, smoothstep(0.26, 0.23, rt) + smoothstep(3.1, 3.80, rt));

  if (piece_number % 2 == 1) {
    float r = sqrt(pow(oPos.x, 2) + pow(oPos.z, 2));
    col += vec3(0.2, 0.3, 0.0) * smoothstep(0.34, 0.30, r);
  }
  return col;
}

const vec3 st_c1 = vec3(0.2, 0.19, 0.02);
const vec3 st_c2 = vec3(0.22, 0.22, 0.14);
const vec3 st_c3 = vec3(0.31, 0.32, 0.30);
const vec3 st_c4 = vec3(0.52, 0.55, 0.49);

vec3 procedural_stone(vec3 pos)
{
  float grid = max(0.0, smoothstep(-0.0001, -0.1, pos.y)
      - smoothstep(5.6, 6.0, max(abs(pos.x), abs(pos.z))));
  //float tu1 = snoise3(7.0 * pos)/* + 0.6 * snoise3(14.0 * pos)*/;
  //tu1 *= 0.6;
  float tu2 =snoise3(7.0 * pos);
  float tu1 = cos(14.0 * pos.x + 19.5*tu2) * sin(21.0 * pos.z + 5.0*tu2) * 0.7;
  tu2 = pow(tu2, 4.0);
  vec3 col = mix(st_c3, st_c4, 0.5 + 0.5 * tu1);
  col -= tu2 * (1.0 - st_c1);
  return mix(col, st_c2, 0.9 * grid);
}

void main()
{
  /*
     final_color = vec4(VS_normal_ws, 1.0);
     return;
   */
  vec3 tex_color;
  vec3 material_ambient_color = vec3(0.5, 0.5, 0.5);
  vec3 material_diffuse_color = vec3(0.9, 0.85, 0.7);
  float alpha = general_alpha;


  if (piece_number == -1) {
    tex_color = procedural_stone(objpos);
    material_shininess = 100.0;
    material_specular_color = vec3(0.3, 0.3, 0.3);
  } else if (piece_number == -2) {
    vec4 c = texture(diffuse_tex, VS_tex_coord);
    tex_color = c.rgb;
    alpha *= c.a;
    material_diffuse_color = vec3(0.2, 0.2, 0.2);
    material_specular_color = vec3(0.5, 0.5, 0.5);
    material_ambient_color = vec3(0.1, 0.1, 0.1);
    material_shininess = 180.0;
  } else if (piece_number == -3) {
    final_color = texture(diffuse_tex, VS_tex_coord);
    final_color.a *= general_alpha;
    return;
  } else if (piece_number == -4) {
    vec4 c = mix(vec4(0.9, 0.25, 0.2, 0.7), vec4(0.4, 0.9, 0.2, 0.7),
        objpos.x);
    final_color = mix(c, vec4(0.1, 0.1, 0.1, 0.5),
        step(general_alpha, objpos.x));
    return;
    /*
  } else if (piece_number > 7) {
    tex_color = procedural_polkagris(objpos);
    material_shininess = 100.0;
    material_specular_color = vec3(0.9, 0.9, 0.9);
    */
  } else {
    tex_color = procedural_wood(objpos);
  }


  vec3 mat_ambient = material_ambient_color * tex_color;
  vec3 mat_diffuse = material_diffuse_color * tex_color;
  vec3 mat_specular = material_specular_color;

  vec3 pos = VS_position_ws;

  vec3 N = normalize(VS_normal_ws);
  if (piece_number == -2) {
    vec3 normap = (2.0 * texture(normal_tex, VS_tex_coord).xyz - 1.0);
    N += normap * pow(dot(N, vec3(0.0, 0.0, 1.0)), 5.0);
  }
  N = normalize(N);

  vec3 Eye = normalize(eye_position - pos);

  vec3 light = mat_ambient * light_ambient_color;

  for (int i = 0; i < n_lights; i++) {
    vec3 L;
    if (light_position[i].w == 0.0)
      L = normalize(light_position[i].xyz);
    else
      L = normalize(light_position[i].xyz - pos);

    vec3 H = normalize(L + Eye);

    float Idiff = max(dot(N, L), 0.0);
    float Ispec = Idiff * pow(max(dot(N, H), 0.0), material_shininess);

    light = light +
      mat_diffuse * light_diffuse_color[i] * Idiff +
      mat_specular * light_specular_color[i] * Ispec;
  }

  for (int i = 0; i < n_spots; i++) {
    vec3 L = normalize(spotlight_position[i].xyz - pos);
    vec3 H = normalize(L + Eye);
    float Idiff = max(dot(N, L), 0.5);
    float Ispec = Idiff * pow(max(dot(N, H), 0.0), material_shininess);
    float spot_effect = max(dot(vec3(0.0, 1.0, 0.0), L), 0.0);
    if (spot_effect > 0.994) {
      spot_effect = smoothstep(0.9941, 0.9948, spot_effect)
      * pow(spot_effect, 200.0) * (1.0 - smoothstep(0.0, -0.3, objpos.y));
      light = light +
        mat_diffuse * spot_effect * Idiff+
        mat_specular * spot_effect * Ispec;
    }
  }

  final_color = vec4(light, alpha);
}

