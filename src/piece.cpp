#include "piece.hpp"

PV112::PV112Geometry Piece::geometries[8];
Piece *Piece::pieces[16];

Piece::Piece(unsigned geom_index, unsigned tex_index) :
  number(8 * tex_index + geom_index),
  geom(&Piece::geometries[geom_index])
{
}

void Piece::load_pieces(int pos_l, int norm_l, int tex_l)
{
  geometries[0b000] = PV112::LoadOBJ("obj/p000s.obj", pos_l, norm_l, tex_l);
  geometries[0b001] = PV112::LoadOBJ("obj/p001s.obj", pos_l, norm_l, tex_l);
  geometries[0b010] = PV112::LoadOBJ("obj/p010s.obj", pos_l, norm_l, tex_l);
  geometries[0b011] = PV112::LoadOBJ("obj/p011s.obj", pos_l, norm_l, tex_l);
  geometries[0b100] = PV112::LoadOBJ("obj/p100s.obj", pos_l, norm_l, tex_l);
  geometries[0b101] = PV112::LoadOBJ("obj/p101s.obj", pos_l, norm_l, tex_l);
  geometries[0b110] = PV112::LoadOBJ("obj/p110s.obj", pos_l, norm_l, tex_l);
  geometries[0b111] = PV112::LoadOBJ("obj/p111s.obj", pos_l, norm_l, tex_l);

  for (int i = 0; i < 8; i++) {
    Piece::pieces[i] = new Piece(i, 0);
    Piece::pieces[i + 8] = new Piece(i, 1);
  }
}

glm::vec3 Piece::get_translation_vector()
{
  glm::vec3 trans;
  trans.x = 3 * board_x - 4.5f;
  trans.y = 0;
  trans.z = 3 * board_y - 4.5f;
  return trans;
}
