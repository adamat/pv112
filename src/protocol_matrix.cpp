#include "game_controller.hpp"
#define _P(x) &ProtoImpl::x

void (ProtoImpl::*(ProtoImpl::proto_matrix)[_MESSAGE_COUNT_][_STATUS_COUNT_])
  (const char*)= {
/*CMD   | S_WAIT4HELLO,      S_PLAYER,          S_SELECTING,       S_PLACING,         S_PLACED_ACK,      S_WON,             S_EXITED   */
/*INVAL*/{_P(cmd_inval),     _P(cmd_inval),     _P(cmd_inval),     _P(cmd_inval),     _P(cmd_inval),     _P(cmd_inval),     _P(cmd_noop)},
/*OKAY.*/{_P(cmd_unimpl),    _P(cmd_player),    _P(cmd_unimpl),    _P(cmd_unimpl),    _P(cmd_place_ack), _P(cmd_unimpl),    _P(cmd_noop)},
/*NOTOK*/{_P(cmd_unimpl),    _P(cmd_player),    _P(cmd_unimpl),    _P(cmd_unimpl),    _P(cmd_unimpl),    _P(cmd_unimpl),    _P(cmd_noop)},
/*QUIT.*/{_P(cmd_quit),      _P(cmd_quit),      _P(cmd_quit),      _P(cmd_quit),      _P(cmd_quit),      _P(cmd_quit),      _P(cmd_noop)},
/*HELP.*/{_P(cmd_help),      _P(cmd_help),      _P(cmd_help),      _P(cmd_help),      _P(cmd_help),      _P(cmd_help),      _P(cmd_noop)},
/*HELLO*/{_P(cmd_hello),     _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_noop)},
/*PLAYR*/{_P(cmd_notnow),    _P(cmd_player),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_noop)},
/*NEGOT*/{_P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_noop)},
/*PIECE*/{_P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_piece),     _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_noop)},
/*PLACE*/{_P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_place),     _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_noop)},
/*RPLAY*/{_P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_replay),    _P(cmd_noop)},
/*PRINT*/{_P(cmd_notnow),    _P(cmd_notnow),    _P(cmd_unimpl),    _P(cmd_unimpl),    _P(cmd_unimpl),    _P(cmd_notnow),    _P(cmd_noop)},
};

