project(Quarto)

set(QUARTO_SRC
  main.cpp
  utils.cpp
  net.cpp
  board.cpp
  piece.cpp
  panel.cpp
  gui.cpp
  bastlaction.cpp
  bastlapi.cpp
  sound.cpp
  game_controller.cpp
  line_buffer.cpp
  PV112.cpp
  )

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread")
# Generate gprof data when compiled in Debug mode
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -pg")
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -pg")

add_executable(quarto ${QUARTO_SRC})
target_link_libraries(quarto GL glut GLEW IL sfml-system sfml-audio)

