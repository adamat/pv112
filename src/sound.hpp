#ifndef SOUND_H_TBORN018
#define SOUND_H_TBORN018

#include <SFML/Audio.hpp>

enum SoundId {
  SND_SELECT = 0,
  SND_PLACE = 1,
  SND_SPOTLIGHT = 2,
  SND_WON = 3,
  SND_UNMUTE = 4,
  SND_TIE = 5
};

#define N_SOUNDS 6

class SoundManager
{
  private:
    float volume = 80.0f;
    float prev_volume = 80.0f;
    sf::SoundBuffer sndbuff[N_SOUNDS];
    sf::Sound snd[N_SOUNDS];

  public:
    SoundManager();
    virtual ~SoundManager();
    void change_volume(float delta);
    void toggle_mute();
    void play_sound(SoundId id);
    float get_volume();
};

extern SoundManager *sound_mgr;

#endif /* end of include guard: SOUND_H_TBORN018 */
