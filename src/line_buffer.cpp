#include "line_buffer.hpp"
#include <unistd.h>
#include <sys/uio.h>

LineBuffer::LineBuffer(unsigned size):
  size(size)
{
  buffer = new char[size];
}

LineBuffer::~LineBuffer()
{
  delete[] buffer;
}

int LineBuffer::read_from_fd(int fd)
{
  unsigned maxlen = size - length;
  unsigned startpos = begin + length;
  int nread;
  if (maxlen == 0)
    return 0;
  if (startpos < size and begin > 0) {
    //need scatter read
    struct iovec iov[2] = {{buffer+startpos, maxlen-begin}, {buffer, begin}};
    nread = readv(fd, iov, 2);
  } else {
    if (begin != 0)
      startpos -= size;
    nread = read(fd, buffer + startpos, maxlen);
  }

  if (nread > 0)
    length += nread;
  return nread;
}

int LineBuffer::pop(char *target)
{
  if (length == 0)
    return 0;

  unsigned i;
  for (i = 0; i < length and at(i) != '\n'; i++) {
    target[i] = at(i);
  }

  length -= (i + 1);
  begin = (length) ? (begin + i + 1) : (0);
  if (begin >= size)
    begin -= size;

  target[i] = '\0';

  if (at(i) == '\n')
    return i + 1; // return length of line (incl. 0)
  else
    return -(i + 1); // return length of written text (no newline found)
}

unsigned LineBuffer::lines_ready()
{
  unsigned n = 0;
  for (unsigned i = 0; i < length; i++)
    n += (at(i) == '\n');
  return n;
}

char LineBuffer::at(unsigned i)
{
  if (i > size - begin)
    return buffer[begin + i - size];
  else
    return buffer[begin + i];
}

