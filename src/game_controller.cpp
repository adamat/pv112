#include "game_controller.hpp"
#include <cstdio>
#include <cstring>
#include <cctype>
#include <iostream>
#include <fcntl.h>
#include <stdexcept>
#include <csignal>
#include <unistd.h>
#include <pwd.h>
#include <sys/select.h>
#include "gui.hpp"
#include "line_buffer.hpp"

#include "protocol_matrix.cpp"

#define OTHER_PLAYER(x) (0x03 bitand (((x) + 1) * 3))

extern int pipe_read_fd;

static void sigpipe_handler(int)
{
  std::cerr << "[game thread] Received SIGPIPE.\n";
}

static void terminator()
{
  std::cout << "QUIT. Terminated." << std::endl;
  exit(0);
}

void game_controller(PlayersLoc pl, int in_fd, int out_fd)
{
  signal(SIGPIPE, sigpipe_handler);
  std::set_terminate(terminator);

  int fnflags = fcntl(in_fd, F_GETFL);
  fcntl(in_fd, F_SETFL, fnflags bitand compl O_DIRECT);

  fd_set rfds;
  ProtoImpl c3po(pl, out_fd);
  LineBuffer lbuff(511);
  char sock_msg[512];
  int nfds = 1 + ((in_fd > pipe_read_fd)?in_fd:pipe_read_fd);
  do {
    FD_ZERO(&rfds);
    FD_SET(pipe_read_fd, &rfds);
    if (pl != BOTH_LOCAL)
      FD_SET(in_fd, &rfds);
    int sel_ret = select(nfds, &rfds, NULL, NULL, NULL);
    if (sel_ret == -1) {
      perror("[C3PO err] select()");
      continue;
    } else {
      if (FD_ISSET(in_fd, &rfds)) {
        if(lbuff.read_from_fd(in_fd) == 0) { // closed or full buffer
          lbuff.pop(sock_msg);
          std::cerr << "[C3PO dbg] server=" << (pl == LISTENER)
            << "; Peer disconnected\n";
          std::cerr << "[C3PO dbg] buffer:" << sock_msg << "<<end>>\n";
          c3po.bastl_says('d');
        } else {
          unsigned lines = lbuff.lines_ready();
          for (unsigned i = 0; i < lines; i++) {
            unsigned len = lbuff.pop(sock_msg);
            c3po.socket_says(sock_msg, len - 1);
          }
        }
      }
      if (FD_ISSET(pipe_read_fd, &rfds)) {
        char c = piperead();
        c3po.bastl_says(c);
      }
    }
  } while (not c3po.exited());
  close(pipe_read_fd);
  std::cerr << "[game thread] Exited normally\n";
}

Game::Game()
{
}

Game::~Game()
{
}

bool Game::try_select_piece(char piece)
{
  if (piece_used[piece])
    return false;
  piece_used[piece] = true;
  selected_piece = piece;
  return true;
}

bool Game::try_place_piece(char position)
{
  if (board[position] != -1)
    return false;
  board[position] = selected_piece;
  w_type = check_win(position);
  return true;
}

WinType Game::did_win()
{
  return w_type;
}

WinType Game::check_win(int pos)
{
  int *b = board;
  /* horizontal */
  int p = (pos / 4) * 4;
  int product = b[p] & b[p+1] & b[p+2] & b[p+3] & 15;
  int nproduct = ~b[p] & ~b[p+1] & ~b[p+2] & ~b[p+3] & 15;
  bool full = (b[p] | b[p+1] | b[p+2] | b[p+3]) >= 0;
  if (full and (product or nproduct))
    return HORIZONTAL;

  /* vertical */
  p = pos % 4;
  product = b[p] & b[p+4] & b[p+8] & b[p+12] & 15;
  nproduct = ~b[p] & ~b[p+4] & ~b[p+8] & ~b[p+12] & 15;
  full = (b[p] | b[p+4] | b[p+8] | b[p+12]) >= 0;
  if (full and (product or nproduct))
    return VERTICAL;

  /* diagonal 0 - 15 */
  if (pos % 5 == 0) {
    product = b[0] & b[5] & b[10] & b[15] & 15;
    nproduct = ~b[0] & ~b[5] & ~b[10] & ~b[15] & 15;
    full = (b[0] | b[5] | b[10] | b[15]) >= 0;
    if (full and (product or nproduct))
      return DIAGONAL;
  }
  /* diagonal 3 - 12 */
  else if (pos % 3 == 0) {
    product = b[3] & b[6] & b[9] & b[12] & 15;
    nproduct = ~b[3] & ~b[6] & ~b[9] & ~b[12] & 15;
    full = (b[3] | b[6] | b[9] | b[12]) >= 0;
    if (full and (product or nproduct))
      return DIAGONAL;
  }

  return NONE;
}

/****************************************************************************/

ProtoImpl::ProtoImpl(PlayersLoc pl, int out_fd)
{
  if (out_fd >= 0 and pl != BOTH_LOCAL) {
    out_f = fdopen(out_fd, "w");
  } else {
    out_f = NULL;
  }

  if (pl == BOTH_LOCAL) {
    local_player = 3;
    state = S_SELECTING;
    bastl_select_piece();
  } else if (pl == LISTENER) {
    local_player = 4; // Need to be negotiated
    state = S_WAIT4HELLO;
    server = true;
    send_hello();
    fflush(out_f);
  } else if (pl == CONNECTOR) {
    local_player = 4; // Need to be negotiated
    state = S_WAIT4HELLO;
    server = false;
  } else if (pl == BOTH_REMOTE) {
    //Not yet or any time soon
    local_player = 0;
    state = S_EXITED;
  }

  game = new Game();
}

ProtoImpl::~ProtoImpl()
{
  delete game;
  if (out_f)
    fclose(out_f);
}

void ProtoImpl::send_hello()
{
  struct passwd *pw = getpwuid(getuid());
  char hostname[128];
  gethostname(hostname, 127);
  hostname[127] = 0;
  fprintf(out_f, "HELLO %s@%s\n", pw->pw_name, hostname);
}

void ProtoImpl::send_integer(const char cmd[5], int i)
{
  if (out_f)
    fprintf(out_f, "%.5s %d\n", cmd, i);
}

void ProtoImpl::send_message(const char *cmd)
{
  if (out_f)
    fprintf(out_f, "%s\n", cmd);
}

void ProtoImpl::try_replay()
{
  if (replay == 3) {
    // obtain preferred player
    pref_player = OTHER_PLAYER(local_player);
    state = S_PLAYER;
    peer_pref = -1;
    local_player = 4; // To be negotiated
    if (server) {
      std::cerr << "[C3PO dbg] Replaying\n";
      fprintf(out_f, "PLAYR %hhd\n", pref_player);
    }
    replay = 0;
  }
}

void ProtoImpl::bastl_says(char msg)
{
  if (msg == 'd' and local_player == 3) {
    // no one cares, this shouldn't happen.
    return;
  } else if (msg == 'q' or msg == 'e' or msg == 'd') {
    std::cerr << "[C3PO dbg] bastl says: " << msg << std::endl;
    cmd_quit(NULL);
    bastl_win(NONE, 1);
    return;
  } else if (msg == 'r' and state == S_WON) {
    delete game;
    game = new Game();
    nowplaying = 1;
    if (local_player != 3) {
      send_message("RPLAY");
      replay |= 1;
      try_replay();
      fflush(out_f);
    } else {
      state = S_SELECTING;
      bastl_select_piece();
    }

    /**************************/
  } else if (state == S_SELECTING and msg != BASTL_ACK) {
    game->try_select_piece(msg);
    bastl_highlight_piece(msg);
    send_integer("PIECE", msg);
    fflush(out_f);

    // We need to wait for bastl's ACK, but it may send 'q' instead.
    // We may not return, because we want to ignore the socket for now.
    bastl_says(piperead());
    /**************************/
  } else if (state == S_SELECTING and msg == BASTL_ACK) {
    state = S_PLACING;
    nowplaying = OTHER_PLAYER(nowplaying); // change current player
    if (nowplaying bitand local_player) {
      bastl_select_position();
    }
    /**************************/
  } else if (state == S_PLACING and msg != BASTL_ACK) {
    game->try_place_piece(msg);
    bastl_place_piece(msg);
    lastpos = msg;
    send_integer("PLACE", msg);
    fflush(out_f);
    bastl_says(piperead());

    /**************************/
  } else if (state == S_PLACING and local_player == nowplaying) {
    // Here, bastl finished animation after *I* clicked; only when online
    state = S_PLACED_ACK; // Waiting for peer's approval.
  } else if (state == S_PLACING) {
    round_count++;

    if (game->did_win() != NONE) {
      state = S_WON;
      bastl_win(game->did_win(), lastpos);
    } else if (round_count == 16) {
      state = S_WON;
      bastl_win(NONE, 0); // Tie
    } else {
      state = S_SELECTING;
      if (nowplaying bitand local_player) {
        bastl_select_piece();
      }
    }
  }
}


void ProtoImpl::socket_says(const char *msg, unsigned len)
{
  std::cerr << "[C3PO dbg] Received: " << msg << std::endl;
  if (len < 5) {
    fprintf(out_f, "INVAL Invalid command - message too short. Strike.\n");
    strikes--;
  } else if (msg[5] and not isspace(msg[5])) {
    fprintf(out_f, "INVAL Invalid command - command too long. Strike.\n");
    strikes--;

  } else {
    int cmdi = cmd_index(msg);
    if (cmdi == -1) {
      fprintf(out_f, "INVAL Invalid command - no such command. Strike.\n");
      strikes--;
    } else {
      // Actual callback
      (*this.*proto_matrix[cmdi][state])(msg);
    }
  }

  if (strikes <= 0) {
    std::cerr << "[C3PO] Too many invalid messages from peer. Closing.\n";
    fprintf(out_f, "QUIT. Strike out.\n");
    //TODO this properly
    state = S_EXITED;
  }

  fflush(out_f);
}

#define INTCMD(_s_) ((uint64_t(_s_[0])<<32L)|(_s_[1]<<24)|(_s_[2]<<16)|(_s_[3]<<8)|_s_[4])

int ProtoImpl::cmd_index(const char cmd[5])
{
    uint64_t icmd = INTCMD(cmd);
    switch (icmd) {
    case INTCMD("INVAL"): return 0;
    case INTCMD("OKAY."): return 1;
    case INTCMD("NOTOK"): return 2;
    case INTCMD("EXIT."):
    case INTCMD("QUIT."): return 3;
    case INTCMD("HELP."): return 4;
    case INTCMD("HELLO"): return 5;
    case INTCMD("PLAYR"): return 6;
    case INTCMD("NEGOT"): return 7;
    case INTCMD("PIECE"): return 8;
    case INTCMD("PLACE"): return 9;
    case INTCMD("RPLAY"): return 10;
    case INTCMD("PRINT"): return 11;
    default:              return -1;
    }
}

bool ProtoImpl::exited()
{
  return state == S_EXITED;
}


/*** Command callbacks ***/

void ProtoImpl::cmd_unimpl(const char *msg)
{
  std::cerr << "[C3PO] Command unimplemented:\n > " << msg << std::endl;
  fprintf(out_f, "INVAL Command unimplemented: %.5s\n", msg);
}


void ProtoImpl::cmd_help(const char *msg)
{
  fprintf(out_f, "OKAY. This is help. You have %hhu strikes left\n", strikes);
}

void ProtoImpl::cmd_quit(const char *msg)
{
  send_message("QUIT. Good bye!\n");
  bastl_win(NONE, 1);
  state = S_EXITED;
}

void ProtoImpl::cmd_noop(const char *msg)
{
  ;
}

void ProtoImpl::cmd_notnow(const char *msg)
{
  fprintf(out_f, "INVAL Not now. \"%.5s\" not expected. Strike.\n", msg);
  strikes--;
}

void ProtoImpl::cmd_inval(const char *msg)
{
  std::cerr << "[C3PO] Peer sent: \"" << msg <<
    "\"\n[C3PO] No error handling implemented; closing communication.\n";
  fprintf(out_f, "QUIT. Unhandled INVAL command.\n");
  state = S_EXITED;
  bastl_win(NONE, 1);
}

void ProtoImpl::cmd_hello(const char *msg)
{
  state = S_PLAYER;
  if (server) { //this is a hello-back
    //TODO: decide which player I want to be
    pref_player = 0;
    fprintf(out_f, "PLAYR %hhd\n", pref_player);
  } else { //we need to send hello back
    send_hello();
  }
}

void ProtoImpl::cmd_player(const char *msg)
{
  uint64_t icmd = INTCMD(msg);

  if (icmd == INTCMD("PLAYR")) {
    char tmp_peer_pref;
    sscanf(msg, "%*s %hhd", &tmp_peer_pref);
    if (peer_pref > 0) { // Peer has already made preference
      fprintf(out_f, "INVAL Changing preference not possible. Strike.\n");
      strikes--;
      return;
    } else if ((server or peer_pref == 0) and tmp_peer_pref == 0) {
      fprintf(out_f, "INVAL Cannot express indifference now. Strike.\n");
      strikes--;
      return;
    } else if (tmp_peer_pref < 0 or tmp_peer_pref > 2) {
      fprintf(out_f,
          "INVAL Possible arguments for 'PLAYR' are 0, 1 and 2. Strike.\n");
      strikes--;
      return;
    } else {
      peer_pref = tmp_peer_pref;
    }

    /* The following will be replaced by user's selection */
    if (pref_player == -1) {
      if (peer_pref == 0) {
        pref_player = 1;
      } else if (peer_pref == 1) {
        pref_player = 2;
      } else if (peer_pref == 2) {
        pref_player = 1;
      }
    }
    /* End of my predefined flexibility */

  } else if (icmd == INTCMD("OKAY.")) {
    if (pref_player == 1) {
      local_player = 1;
      peer_pref = 2;
    } else {
      fprintf(out_f, "INVAL Only \"PLAYR 1\" can be okayed. Strike.\n");
      strikes--;
      return;
    }
  } else if (icmd == INTCMD("NOTOK")) {
    if (peer_pref == -1) {
      peer_pref = pref_player;
    } else {
      fprintf(out_f, "INVAL Only first \"PLAYR\" can be disputed. Strike.\n");
      strikes--;
      return;
    }
  }

  if (peer_pref != pref_player) {
    if (peer_pref == 1) {
      fprintf(out_f, "OKAY. Your turn.\n");
      strikes = 10; // Reset strikes after succesful negotiation
      localplayer = 1; // For bastl
      local_player = 2;
      state = S_SELECTING;
    } else if (pref_player == 2) {
      fprintf(out_f, "PLAYR 2 Please confirm by \"PLAYR 1\".\n");
    } else if (peer_pref == 2 and pref_player == 0) {
      fprintf(out_f, "PLAYR 1 I'll take it.\n");
      pref_player = 1;
    } else if (pref_player == 1) {
      if (local_player == 1) {
        // Start playing.
        strikes = 10; // Reset strikes after succesful negotiation
        localplayer = 0; // For bastl
        state = S_SELECTING;
        bastl_select_piece();
      }
      else
        fprintf(out_f, "PLAYR 1\n");
    }
  } else {
    fprintf(out_f, "INVAL Sorry, I don't know how to negotiate. Strike.\n");
    peer_pref = -1;
    strikes--;
  }
}

void ProtoImpl::cmd_piece(const char *msg)
{
  if (nowplaying == local_player) {
    fprintf(out_f, "INVAL Not your turn. Strike\n");
    strikes--;
    return;
  }
  int p;
  sscanf(msg, "%*s %d", &p);
  if (p < 0 or p > 15) {
    fprintf(out_f,
        "INVAL Possible arguments for 'PIECE' are 0 - 15. Strike.\n");
    strikes--;
  } else if (game->try_select_piece(p)) {
    bastl_highlight_piece(p);
  } else {
    fprintf(out_f, "INVAL Piece %d is already used. Strike.\n", p);
    strikes--;
  }
}

void ProtoImpl::cmd_place(const char *msg)
{
  if (nowplaying == local_player) {
    fprintf(out_f, "INVAL Not your turn. Strike\n");
    strikes--;
    return;
  }
  int p;
  sscanf(msg, "%*s %d", &p);
  if (p < 0 or p > 15) {
    fprintf(out_f,
        "INVAL Possible arguments for 'PLACE' are 0 - 15. Strike.\n");
    strikes--;
  } else if (game->try_place_piece(p)) {
    bastl_place_piece(p);
    lastpos = p;
    fprintf(out_f, "OKAY.\n");
  } else {
    fprintf(out_f, "INVAL Position %d is already taken. Strike.\n", p);
    strikes--;
  }
}

void ProtoImpl::cmd_place_ack(const char *msg)
{
  round_count++;
  if (game->did_win() != NONE) {
    state = S_WON;
    bastl_win(game->did_win(), lastpos);
  } else if (round_count == 16) {
    state = S_WON;
    bastl_win(NONE, 0); // Tie
  } else {
    state = S_SELECTING;
    if (nowplaying bitand local_player) {
      bastl_select_piece();
    }
  }
}

void ProtoImpl::cmd_replay(const char *msg)
{
  replay |= 2;
  try_replay();
}

