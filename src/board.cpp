#include <algorithm>
#include <functional>
#include "board.hpp"
#include "sound.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/freeglut.h>
#include "utils.hpp"
#include "piece.hpp"
#include "bastlaction.hpp"

using std::min;
using std::max;

const char *OBJ_FILE_BOARD = "obj/board.obj";
const float A_WIN_DURATION = 1.0f;

static void spotlight_switcher(int n_spots)
{
  if (bastl_status == WON) {
    board.active_spotlights = n_spots;
    sound_mgr->play_sound(SND_SPOTLIGHT);
  }
}

Board::Board() :
  pieces{nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
    nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
    nullptr},
    spotlit_pieces{0, 0, 0, 0}
{
}

Board::Board(int pos_l, int norm_l, int tex_l) :
  Board()
{
  board_geom = PV112::LoadOBJ(OBJ_FILE_BOARD, pos_l, norm_l, tex_l);
}

Board::~Board()
{
}

void Board::reinit()
{
  memset(pieces, 0, 16 * sizeof(Piece*));
  memset(spotlit_pieces, 0, 4 * sizeof(int));
  active_spotlights = 0;
  hover = nullptr;
  piece_to_place = nullptr;
  win_anim = 0.0f;
}

void Board::draw(float frtime)
{
  /***** Update *****/
  if (piece_to_place) {
    pieces[pos_to_place] = piece_to_place;
    piece_to_place->board_x = pos_to_place % 4;
    piece_to_place->board_y = pos_to_place / 4;
    piece_to_place = nullptr;
  }

  if (bastl_status == WON or bastl_status == TIE or bastl_status == CONNERROR){
    if (win_anim == 0.0f) {
      win_anim = 0.0001f;
      if (bastl_status == WON) {
        glutTimerFunc(1500, spotlight_switcher, 1);
        glutTimerFunc(2000, spotlight_switcher, 2);
        glutTimerFunc(2500, spotlight_switcher, 3);
        glutTimerFunc(3000, spotlight_switcher, 4);
        glutTimerFunc(3500,
            [](int x){
            if(bastl_status == WON) {
              gui->bastl_status_changed(WON);
              if(localplayer == -1 or localplayer == player)
                sound_mgr->play_sound(SND_WON);
              else
                sound_mgr->play_sound(SND_TIE);
            }
            }, WON);
      } else {
        glutTimerFunc(500,
            [](int x){
            if(bastl_status == TIE or bastl_status == CONNERROR) {
              sound_mgr->play_sound(SND_TIE);
            }
            }, TIE);
      }
    } else if (win_anim < 1.0f) {
      win_anim = min(1.0f, win_anim + frtime / A_WIN_DURATION);
    }
  }

  /*** Use Program **/

  glViewport(0, 0, win_width, win_height);

  glUniform3fv(eye_position_loc, 1, glm::value_ptr(camera.GetEyePosition()));

  glUniform3f(light_ambient_color_loc, 0.3f, 0.3f, 0.3f);

  float Idiff = 1.0 - 0.9 * win_anim * win_anim;
  glUniform4f(light_position_loc, 0.0f, 15.0f, 10.0f, 0.0f);
  glUniform3f(light_diffuse_color_loc, Idiff, Idiff, Idiff);
  glUniform3f(light_specular_color_loc, Idiff, Idiff, Idiff);

  glm::vec3 col;
  glUniform4f(1 + light_position_loc, 0, 20.0f, 7.0f, 1.0f);
  if (bastl_status == PLACING) {
    col = .25f * (1.f + sinf(5 * app_time_s)) * player_color[player];
  } else if (bastl_status == WON) {
    col = .3f * win_anim * win_anim * win_anim * player_color[player];
    glClearColor(0.1f * col.r, 0.1f * col.g, 0.1f * col.b, 1.0f);
  } else if (bastl_status == TIE) {
    col = .3f * win_anim * win_anim * win_anim * glm::vec3(0.92f, 1.0f, 0.0f);
    glClearColor(0.1f * col.r, 0.1f * col.g, 0.1f * col.b, 1.0f);
  } else if (bastl_status == CONNERROR) {
    col = .3f * win_anim * win_anim * win_anim * glm::vec3(0.92f, 0.2f, 0.85f);
    glClearColor(0.1f * col.r, 0.1f * col.g, 0.1f * col.b, 1.0f);
  } else {
    col = {0.0f, 0.0f, 0.0f};
  }
  glUniform3fv(1 + light_diffuse_color_loc, 1, glm::value_ptr(col));
  glUniform3fv(1 + light_specular_color_loc, 1, glm::value_ptr(col));

  /***** Render *****/
  model_matrix = glm::translate(
      glm::rotate(glm::mat4(1.0f), rotation, glm::vec3(0, 1, 0)),
      glm::vec3(0, -1.5f, 0));
  view_matrix = glm::lookAt(camera.GetEyePosition(), glm::vec3(0.0f, 0.0f,
        0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
  glm::mat3 normal_matrix;

  float aspect = float(win_width) / float(win_height);
  projection_matrix = glm::translate(glm::mat4(1),
      glm::vec3(-0.9f * (0.6f / aspect), -0.14f, 0))
    * glm::scale(glm::mat4(1.0), glm::vec3(min(1.0f, 0.65f * aspect)))
    * glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f);

  glBindVertexArray(board_geom.VAO);

  glUniform1f(general_alpha_loc, 1.0f);

  PVM_matrix = projection_matrix * view_matrix * model_matrix;
  normal_matrix = glm::inverse(glm::transpose(glm::mat3(model_matrix)));

  glUniform1i(n_spotlights_loc, active_spotlights);
  for (int i = 0; i < active_spotlights; i++) {
    glm::vec4 spot_pos((spotlit_pieces[i] % 4) * 3.0 - 4.5, 20.0f,
        (spotlit_pieces[i] / 4) * 3.0 - 4.5, 1.0);
    spot_pos = model_matrix * spot_pos;
    glUniform4fv(i + spotlight_position_loc, 1, glm::value_ptr(spot_pos));
  }


  glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(model_matrix));
  glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(PVM_matrix));
  glUniformMatrix3fv(normal_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(normal_matrix));

  glUniform1i(piece_number_loc, -1);
  PV112::DrawGeometry(board_geom);

  /****** Draw pieces *******/
  for(Piece *p : pieces){
    if (p) {
      glBindVertexArray(p->geom->VAO);
      glm::mat4 piece_model_matrix = glm::translate(model_matrix,
          p->get_translation_vector());
      PVM_matrix = projection_matrix * view_matrix * piece_model_matrix;
      normal_matrix = glm::inverse(glm::transpose(
            glm::mat3(piece_model_matrix)));

      glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
          glm::value_ptr(piece_model_matrix));
      glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
          glm::value_ptr(PVM_matrix));
      glUniformMatrix3fv(normal_matrix_loc, 1, GL_FALSE,
          glm::value_ptr(normal_matrix));

      glUniform1i(piece_number_loc, p->number);

      PV112::DrawGeometry(*(p->geom));
    }
  }

  if (hover) {
    glUniform1f(general_alpha_loc, 0.5f);

    glBindVertexArray(hover->geom->VAO);
    glm::mat4 piece_model_matrix = glm::translate(model_matrix,
        hover->get_translation_vector());
    PVM_matrix = projection_matrix * view_matrix * piece_model_matrix;
    normal_matrix = glm::inverse(glm::transpose(glm::mat3(piece_model_matrix)));

    glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(piece_model_matrix));
    glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(PVM_matrix));
    glUniformMatrix3fv(normal_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(normal_matrix));

    glUniform1i(piece_number_loc, hover->number);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthMask(GL_FALSE);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    PV112::DrawGeometry(*(hover->geom));
    glDisable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
  }

  glBindVertexArray(0);
}

void Board::set_rotation(float r)
{
  rotation = r;
}

void Board::place_piece(Piece *p, int position)
{
  piece_to_place = p;
  pos_to_place = position;
  sound_mgr->play_sound(SND_PLACE);
}

void Board::hover_piece(Piece *p, int position)
{
  hover = p;
  if (p) {
    p->board_x = position % 4;
    p->board_y = position / 4;
  }
}

bool Board::is_empty(int position)
{
  return pieces[position] == nullptr;
}

int Board::coords_to_square(float x, float z)
{
  if (x < -6.0f or x > 6.0f or z < -6.0f or z > 6.0f)
    return -1;
  int xi = (x + 6.0f) / 3.0;
  int yi = (z + 6.0f) / 3.0;
  return yi * 4 + xi;
}
