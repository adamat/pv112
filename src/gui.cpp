#include <algorithm>
#include <GL/glew.h>
#include "sound.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "gui.hpp"
#include "utils.hpp"
#include "PV112.h"

using std::min;

GUI::GUI(int position_loc, int tex_coord_loc):
  tex_coord_location(tex_coord_loc)
{
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo_positions);
  glGenBuffers(1, &vbo_texture_coords);

  glBindVertexArray(vao);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_positions);
  glBufferData(GL_ARRAY_BUFFER, 33 * sizeof(float), positions, GL_STATIC_DRAW);
  glEnableVertexAttribArray(position_loc);
  glVertexAttribPointer(position_loc, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_coords);
  glBufferData(GL_ARRAY_BUFFER, 22 * sizeof(float), texture_coords,
      GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray(tex_coord_loc);
  glVertexAttribPointer(tex_coord_loc, 2, GL_FLOAT, GL_FALSE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  diffuse_texture = PV112::CreateAndLoadTexture("gui.png");
  glBindTexture(GL_TEXTURE_2D, diffuse_texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  spinner_texture = PV112::CreateAndLoadTexture("spinner.png");
  glBindTexture(GL_TEXTURE_2D, spinner_texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);
}

GUI::~GUI()
{
}

void GUI::draw()
{
  if (needchange) {
    needchange = false;
    glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_coords);
    glBufferSubData(GL_ARRAY_BUFFER, 0, 8 * sizeof(float), texture_coords);
    //glEnableVertexAttribArray(tex_coord_loc);
    //glVertexAttribPointer(tex_coord_loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_DEPTH_TEST);

  /* Volume bar */
  glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f),
      glm::vec3(-1.0f, -0.99f, 0.5f)) * glm::scale(glm::mat4(1.0f),
      glm::vec3(0.20f));

  glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(model_matrix));
  glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(model_matrix));

  glUniform1i(piece_number_loc, -4);
  glUniform1f(general_alpha_loc, sound_mgr->get_volume() / 100.0f);
  glBindVertexArray(vao);
  glDrawArrays(GL_TRIANGLES, 4, 3);

  /* Status box */
  glViewport(0, 0, win_width, win_height);
  glUniform1i(piece_number_loc, -3);
  glUniform1f(general_alpha_loc, 1.0f);

  glUniform1i(texture_loc, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, diffuse_texture);

  model_matrix = glm::translate(glm::mat4(1.0f),
      glm::vec3(-1.0f, 1.0f, 0.0f)) * glm::scale(glm::mat4(1.0f),
      glm::vec3(0.8f, 0.8f * (float(win_width)/float(win_height)), 1.0f));

  glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(model_matrix));
  glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(model_matrix));

  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  /* Spinner */

  if (bastl_status == IDLE) {
    //glUniform1i(texture_loc, 0);
    //glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, spinner_texture);
    glUniform1f(general_alpha_loc, min(1.0f, (app_time_s - idled) / 1.5f));

    model_matrix = glm::translate(glm::mat4(1.0f),
        glm::vec3(-0.20f, 1.0f, 0.0f))
      * glm::scale(glm::mat4(1.0f),
          glm::vec3(0.1f, 0.1f * (float(win_width)/float(win_height)), 1.0f))
      * glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, -0.90f, 0.0f))
      * glm::rotate(glm::mat4(1.0), -6.5f * app_time_s,
          glm::vec3(0.0f, 0.0f, 1.0f));

    glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(model_matrix));
    glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(model_matrix));

    glDrawArrays(GL_TRIANGLE_STRIP, 7, 4);
  }

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glBindVertexArray(0);
}

/* Might run in game thread! */
void GUI::bastl_status_changed(BastlStatus status)
{
  float y1;
  float y2;
  switch (status) {
    case SELECTING:
      y1 = 1.0f * 0.125f;
      y2 = 0.0f;
      break;
    case PLACING:
      y1 = 3.0f * 0.125f;
      y2 = 2.0f * 0.125f;
      break;
    case WON:
      y1 = 5.0f * 0.125f;
      y2 = 4.0f * 0.125f;
      break;
    case TIE:
      y1 = 7.0f * 0.125f;
      y2 = 6.0f * 0.125f;
      break;
    case CONNERROR:
      y1 = 8.0f * 0.125f;
      y2 = 7.0f * 0.125f;
      break;
    case IDLE:
      idled = app_time_s;
    default:
      return;
  }
  if (player and status != TIE and status != CONNERROR) {
    y1 += 0.125f;
    y2 += 0.125f;
  }
  texture_coords[1] = y1;
  texture_coords[3] = y1;
  texture_coords[5] = y2;
  texture_coords[7] = y2;

  needchange = true;
}

