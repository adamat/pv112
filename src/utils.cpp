#include <iostream>
#include "sound.hpp"
#include <GL/glew.h>
#include <IL/il.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>
#include <unistd.h>
#include <csignal>
#include "PV112.h"
#include "utils.hpp"
#include "piece.hpp"
#include "bastlaction.hpp"
#include "game_controller.hpp"
#include "net.hpp"

float app_time_s = 0.0f;

GLuint program;
GLint model_matrix_loc;
GLint PVM_matrix_loc;
GLint normal_matrix_loc;
GLint general_alpha_loc;
GLint light_position_loc;
GLint light_diffuse_color_loc;
GLint light_ambient_color_loc;
GLint light_specular_color_loc;
GLint spotlight_position_loc;
GLint n_spotlights_loc;
GLint eye_position_loc;
GLint texture_loc;
GLint normal_texture_loc;
GLint piece_number_loc;

glm::vec3 player_color[2] = {{0.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f}};

Board board;
Panel *panel;
GUI *gui;
SoundManager *sound_mgr;
PV112::PV112Camera camera;

extern int pipe_write_fd;
extern int pipe_read_fd;

static void sigpipe_handler(int)
{
  std::cerr << "[bastl thread] Received SIGPIPE.\n";
}

bool process_arguments(int argc, char **argv, PlayersLoc *players_loc,
    int *in_fd, int *out_fd)
{
  if (argc == 1)
    return true;

  if (!strcmp(argv[1], "-s")) {
    *players_loc = LISTENER;
    if (argc > 2 and !strcmp(argv[2], "-")) {
      *in_fd = STDIN_FILENO;
      *out_fd = STDOUT_FILENO;
    } else {
      int port = PORT_DEFAULT;
      if (argc > 2)
        port = atoi(argv[2]);
      else
        fprintf(stderr, "Listening on default port %d\n", port);

      if (!port) {
        fputs("Not a port number\n", stderr);
        return 1;
      }

      *in_fd = *out_fd = listen_and_accept(port);
      if (*in_fd == -1) {
        perror("Cannot open internet socket");
        exit(1);
      }
    }
  } else if (!strcmp(argv[1], "-r")) {
    *players_loc = CONNECTOR;
    if (argc == 2) {
      *in_fd = STDIN_FILENO;
      *out_fd = STDOUT_FILENO;
    } else {
      int port = PORT_DEFAULT;
      if (argc > 3)
        port = atoi(argv[3]);

      if (!port) {
        fputs("Not a port number\n", stderr);
        return 1;
      }

      *out_fd = *in_fd = connect_to(argv[2], port);
      if (*in_fd == -1) {
        perror("Cannot open internet socket");
        exit(1);
      }
    }
  } else {
    return false;
  }
  return true;
}

void print_usage()
{
  fprintf(stderr, "USAGE:\n\tquarto\n\tquarto -r [host [port]]\n"
      "\tquarto -s [port | -]\n\nARGUMENTS:\n"
      "\t -r [host [port]]\n"
      "\t\tIf no host given, assumes a listener on stdout and stdin.\n"
      "\t\tOtherwise tries to connect to host:port and assumes\n"
      "\t\ta listener there. Default port (if no given) is %d.\n"
      "\t -s [port | -]\n\t\t"
      "listens on port (%d if no port number is given) or\n"
      "\t\t(when '-' given) on stdin.\n"
      "\tWhen run without arguments, starts a local two-player game.\n\n",
      PORT_DEFAULT, PORT_DEFAULT);
  }

void init()
{
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearDepth(1.0);
  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);

  // Create shader program
  int position_loc, normal_loc, tex_coord_loc;
  init_shading_stuff(position_loc, normal_loc, tex_coord_loc);

  signal(SIGPIPE, sigpipe_handler);

  board = Board(position_loc, normal_loc, tex_coord_loc);
  Piece::load_pieces(position_loc, normal_loc, tex_coord_loc);
  gui = new GUI(position_loc, tex_coord_loc);
  sound_mgr = new SoundManager();
  panel->geom = PV112::LoadOBJ("obj/panel.obj", position_loc, normal_loc,
      tex_coord_loc);

  glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
}

void reinit()
{
  board.reinit();
  panel->reinit();
  player = 0;
  app_time_s = 0;
  change_status(IDLE);
}

void init_shading_stuff(int &position_loc, int &normal_loc,
    int &tex_coord_loc)
{
  program = PV112::CreateAndLinkProgram("vertex.glsl", "fragment.glsl");
  if (0 == program)
    PV112::WaitForEnterAndExit();

  // Get attribute and uniform locations
  position_loc = glGetAttribLocation(program, "position");
  normal_loc = glGetAttribLocation(program, "normal");
  tex_coord_loc = glGetAttribLocation(program, "tex_coord");

  model_matrix_loc = glGetUniformLocation(program, "model_matrix");
  PVM_matrix_loc = glGetUniformLocation(program, "PVM_matrix");
  normal_matrix_loc = glGetUniformLocation(program, "normal_matrix");

  general_alpha_loc = glGetUniformLocation(program, "general_alpha");

  light_position_loc = glGetUniformLocation(program, "light_position");
  light_ambient_color_loc = glGetUniformLocation(program, "light_ambient_color");
  light_diffuse_color_loc = glGetUniformLocation(program, "light_diffuse_color");
  light_specular_color_loc = glGetUniformLocation(program, "light_specular_color");
  spotlight_position_loc = glGetUniformLocation(program, "spotlight_position");
  n_spotlights_loc = glGetUniformLocation(program, "n_spots");

  eye_position_loc = glGetUniformLocation(program, "eye_position");

  texture_loc = glGetUniformLocation(program, "diffuse_tex");
  normal_texture_loc = glGetUniformLocation(program, "normal_tex");
  piece_number_loc = glGetUniformLocation(program, "piece_number");

  glUseProgram(program);
}

void key_pressed(unsigned char key, int mouseX, int mouseY)
{
  switch (key) {
  case 27:
  case 'q':
    pipewrite('q');
    glutLeaveMainLoop();
    break;
  case 'l':
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutPostRedisplay();
    break;
  case 'L':
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glutPostRedisplay();
    break;
  case 'f':
    glutFullScreenToggle();
    break;
  case 'r':
    if (bastl_status == WON or bastl_status == TIE) {
      reinit();
      pipewrite('r');
    }
    break;
  case 'm':
    sound_mgr->toggle_mute();
    break;
  case '+':
    sound_mgr->change_volume(10.0f);
    break;
  case '-':
    sound_mgr->change_volume(-10.0f);
    break;
  }
}

void render()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  static auto t1 = std::chrono::high_resolution_clock::now();
  auto t2 = std::chrono::high_resolution_clock::now();
  float frame_time = (std::chrono::duration<float>(t2 - t1)).count();

  app_time_s += frame_time;
  board.draw(frame_time);
  panel->draw(frame_time);
  gui->draw();
  t1 = t2;

  // Swaps the front and back buffer (double-buffering)
  glutSwapBuffers();
}

void reshape(int width, int height)
{
  win_width = width;
  win_height = height;
}

void mouse_button_changed(int button, int state, int x, int y)
{
  if (button == GLUT_LEFT_BUTTON) {
    if (state == GLUT_UP)
      return;
    if (bastl_status == PLACING)
      placing_clicked(x, y);
    else if (bastl_status == SELECTING)
      selecting_clicked(x, y);
  } else {
    camera.OnMouseBottomChanged(button, state, x, y);
  }
}

void mouse_moved(int x, int y)
{
  camera.OnMouseMoved(x, y);
  board.set_rotation(camera.angle_direction);
}

void mouse_hover(int x, int y)
{
  if (bastl_status == PLACING)
    placing_hover(x, y);
  else if (bastl_status == SELECTING)
    panel->hover(x, y);
}

void simple_debug_callback(GLenum source, GLenum type, GLuint id, GLenum
    severity, GLsizei length, const char* message, const void* userParam)
{
  switch (type) {
    case GL_DEBUG_TYPE_ERROR:
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      std::cerr << message << std::endl;
      return;
    default:
      return;
  }
}

void timer(int)
{
  glutPostRedisplay();
  glutTimerFunc(20, timer, 0);
}

