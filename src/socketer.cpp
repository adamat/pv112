#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>

int doruska_fd[2];
int donemecka_fd[2];

int main(int argc, const char *argv[]){
  if (pipe(doruska_fd) or pipe(donemecka_fd)) {
    perror("Unable to create pair of pipes");
    return 1;
  }

  close(0);
  close(1);
  int f = fork();
  if (f == -1) {
    perror("Cannot fork");
    return 1;
  } else if (f == 0){
    dup2(doruska_fd[0], 0);
    dup2(donemecka_fd[1], 1);
    close(doruska_fd[0]);
    close(doruska_fd[1]);
    close(donemecka_fd[0]);
    close(donemecka_fd[1]);
    execl("quarto", "quarto", "-r", NULL);
  } else {
    dup2(donemecka_fd[0], 0);
    dup2(doruska_fd[1], 1);
    close(donemecka_fd[0]);
    close(donemecka_fd[1]);
    close(doruska_fd[0]);
    close(doruska_fd[1]);
    execl("quarto", "quarto", "-s", "-", NULL);
  }
  perror("Cannot execute 'quarto'");
  return 1;
}
