#ifndef GAME_CONTROLLER_H_BP26C8QC
#define GAME_CONTROLLER_H_BP26C8QC

#include <cstdio>
#include <string>
#include "bastlapi.hpp"

enum PlayersLoc
{
  BOTH_LOCAL,
  LISTENER,
  CONNECTOR,
  BOTH_REMOTE //longer-term goal
};

class Game
{
  private:
    int board[16] =
      {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
    bool piece_used[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int selected_position = -1;
    int selected_piece = -1;
    WinType w_type = NONE;

    WinType check_win(int pos);

  public:
    Game();
    virtual ~Game ();

    bool try_select_piece(char piece);
    bool try_place_piece(char position);
    WinType did_win();
};

enum CommunicationState
{
  S_WAIT4HELLO,
  S_PLAYER,
  S_SELECTING,
  S_PLACING,
  S_PLACED_ACK,
  S_WON,
  S_EXITED,
  _STATUS_COUNT_
};

const int _MESSAGE_COUNT_ = 12;


class ProtoImpl
{
  private:
    CommunicationState state;
    char lastpos;
    unsigned char round_count = 0;
    unsigned char local_player;
    unsigned char nowplaying = 1;
    unsigned char strikes = 10; // number of possible invalid inputs
    char pref_player = -1;
    char peer_pref = -1;
    char replay = 0;
    bool server;
    Game *game;
    FILE *out_f;

    static int cmd_index(const char cmd[5]);
    void send_hello();
    void send_integer(const char cmd[5], int i);
    void send_message(const char *cmd);
    void try_replay();

    /* command callbacks */
    static
      void (ProtoImpl::*proto_matrix[_MESSAGE_COUNT_][_STATUS_COUNT_])(const char*);
    void cmd_unimpl(const char *msg);
    void cmd_help(const char *msg);
    void cmd_quit(const char *msg);
    void cmd_noop(const char *msg);
    void cmd_notnow(const char *msg);
    void cmd_inval(const char *msg);
    void cmd_hello(const char *msg);
    void cmd_player(const char *msg);
    void cmd_piece(const char *msg);
    void cmd_place(const char *msg);
    void cmd_place_ack(const char *msg);
    void cmd_replay(const char *msg);

  public:
    ProtoImpl(PlayersLoc pl, int out_fd);
    ~ProtoImpl();
    void bastl_says(char msg);
    void socket_says(const char *msg, unsigned len);
    bool exited();
};


void game_controller(PlayersLoc pl, int in_fd, int out_fd);

#endif /* end of include guard: GAME_CONTROLLER_H_BP26C8QC */
