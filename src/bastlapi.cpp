#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <iostream>
#include <stdexcept>
#include "bastlapi.hpp"
#include "bastlaction.hpp"
#include "utils.hpp"

int pipe_read_fd;

char piperead()
{
  char res = 'e';
  int num;
  read(pipe_read_fd, &res, 1);
  return res;
}

void bastl_select_piece()
{
  change_status(SELECTING);
  //return piperead();
}

void bastl_select_position()
{
  change_status(PLACING);
  //return piperead();
}

void bastl_highlight_piece(char piece)
{
  do_highlight(piece);
  //piperead();
};

void bastl_place_piece(char position)
{
  do_place(position);
  //piperead();
}

void bastl_win(WinType type, int position)
{
  int lit[4];
  int pos, k;
  switch (type) {
    case HORIZONTAL:
      pos = (position / 4) * 4;
      lit[0] = pos;
      lit[1] = pos + 1;
      lit[2] = pos + 2;
      lit[3] = pos + 3;
      break;
    case VERTICAL:
      pos = position % 4;
      lit[0] = pos;
      lit[1] = pos + 4;
      lit[2] = pos + 8;
      lit[3] = pos + 12;
      break;
    case DIAGONAL:
      pos = (position % 5)?3:0;
      k = (position % 5)?3:5;
      lit[0] = pos;
      lit[1] = pos + k;
      lit[2] = pos + 2 * k;
      lit[3] = pos + 3 * k;
      break;
    case NONE:
      if (position != 0) {
        change_status(CONNERROR);
        return;
      }
      //no break
    default:
      change_status(TIE);
      return;
      break;
  }
  memcpy(board.spotlit_pieces, lit, 4 * sizeof(int));
  change_status(WON);
}

