#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "net.hpp"

int listen_and_accept(int port)
{
  struct sockaddr_in my_addr, peer_addr;
  int n;
  int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_fd == -1)
    return -1;

  bzero(&my_addr, sizeof(my_addr));
  my_addr.sin_family = AF_INET;
  my_addr.sin_addr.s_addr = INADDR_ANY;
  my_addr.sin_port = htons(port);

  if (bind(sock_fd, (struct sockaddr *) &my_addr, sizeof(my_addr)) == -1)
    return -1;
  listen(sock_fd, 1);

  unsigned peer_addr_len = sizeof(peer_addr);

  int peer_fd = accept(sock_fd, (struct sockaddr*) &peer_addr, &peer_addr_len);

  if (peer_fd == -1)
    return -1;

  struct hostent *peer_info = gethostbyaddr(&peer_addr.sin_addr,
      sizeof(peer_addr.sin_addr), AF_INET);
  if (!peer_info) {
    fprintf(stderr, "[Bastl] Connection from %s:%d\n",
        inet_ntoa(peer_addr.sin_addr), peer_addr.sin_port);
    herror("[Bastl] Cannot resolve");
  } else {
    fprintf(stderr, "[Bastl] Connection from %s (%s:%d)\n",
        peer_info->h_name, inet_ntoa(peer_addr.sin_addr),
        ntohs(peer_addr.sin_port));
  }

  close(sock_fd);
  return peer_fd;
}

int connect_to(const char *hname, int port)
{
  int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_fd < 0)
    return -1;
  struct hostent *peer_host = gethostbyname(hname);
  if (!peer_host) {
    herror("[Bastl] Cannot resolve");
    return -1;
  }

  struct sockaddr_in peer_addr;
  bzero(&peer_addr, sizeof(peer_addr));
  peer_addr.sin_family = AF_INET;
  bcopy(peer_host->h_addr, &peer_addr.sin_addr.s_addr, peer_host->h_length);
  peer_addr.sin_port = htons(port);

  fprintf(stderr, "[Bastl] Connecting to %s (%s:%d)...\n",
        peer_host->h_name, inet_ntoa(peer_addr.sin_addr), port);

  if (connect(sock_fd, (struct sockaddr*) &peer_addr, sizeof(peer_addr)) == -1)
    return -1;
  else
    return sock_fd;
}
