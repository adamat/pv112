#include "sound.hpp"

SoundManager::SoundManager()
{
  if (not sndbuff[0].loadFromFile("sfx/selected.ogg"))
    perror("Error loading \"sfx/selected.ogg\"");
  else
    snd[0].setBuffer(sndbuff[0]);
  if (not sndbuff[1].loadFromFile("sfx/placed.ogg"))
    perror("Error loading \"sfx/placed.ogg\"");
  else
    snd[1].setBuffer(sndbuff[1]);
  if (not sndbuff[2].loadFromFile("sfx/spotlit.ogg"))
    perror("Error loading \"sfx/spotlit.ogg\"");
  else
    snd[2].setBuffer(sndbuff[2]);
  if (not sndbuff[3].loadFromFile("sfx/won.ogg"))
    perror("Error loading \"sfx/won.ogg\"");
  else
    snd[3].setBuffer(sndbuff[3]);
  if (not sndbuff[4].loadFromFile("sfx/unmute.flac"))
    perror("Error loading \"sfx/unmute.flac\"");
  else
    snd[4].setBuffer(sndbuff[4]);
  if (not sndbuff[5].loadFromFile("sfx/tie.ogg"))
    perror("Error loading \"sfx/tie.ogg\"");
  else
    snd[5].setBuffer(sndbuff[5]);
}

SoundManager::~SoundManager()
{
}

void SoundManager::change_volume(float delta)
{
  volume += delta;
  if (volume > 100.0f)
    volume = 100.0f;
  else if (volume < 0.0f)
    volume = 0.0f;

  for(int i = 0; i < N_SOUNDS; i++){
    snd[i].setVolume(volume);
  }

  play_sound(SND_UNMUTE);
}

void SoundManager::toggle_mute()
{
  if (volume == 0.0f) {
    change_volume(prev_volume);
  } else {
    prev_volume = volume;
    change_volume(-volume);
  }
}

void SoundManager::play_sound(SoundId id)
{
  snd[id].play();
}

float SoundManager::get_volume()
{
  return volume;
}

