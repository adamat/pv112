#ifndef LINE_BUFFER_H_T9WVLNIX
#define LINE_BUFFER_H_T9WVLNIX

class LineBuffer
{
  private:
    unsigned size;
    unsigned begin = 0;
    unsigned length = 0;
    char *buffer;

    char at(unsigned i);
  public:
    LineBuffer(unsigned size);
    virtual ~LineBuffer();

    int read_from_fd(int fd);
    int pop(char *target);
    unsigned lines_ready();
};

#endif /* end of include guard: LINE_BUFFER_H_T9WVLNIX */
