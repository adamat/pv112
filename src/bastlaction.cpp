#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <unistd.h>
#include "utils.hpp"
#include "bastlaction.hpp"
#include "piece.hpp"
#include "panel.hpp"

int pipe_write_fd;
int hover_piece;

BastlStatus bastl_status = IDLE;
bool player = 0;
int localplayer = -1;

void change_status(BastlStatus status)
{
  bastl_status = status;
  if (status != WON) // this case is handled differently
    gui->bastl_status_changed(status);
}

void pipewrite(char res)
{
  int num = write(pipe_write_fd, &res, 1);
  if (num < 1) {
    perror("[bastl thread] pipewrite()");
    change_status(CONNERROR);
  }
}

glm::vec4 win_to_world(int x, int y)
{
  GLfloat depth;
  glReadPixels(x, win_height - y - 1, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT,
      &depth);
  glm::vec4 viewport(0, 0, win_width, win_height);
  glm::vec3 win_coords(x, win_height - y - 1, depth);
  glm::vec4 obj_coords(glm::unProject(win_coords,
      board.view_matrix * board.model_matrix,
      board.projection_matrix, viewport), depth);
  return obj_coords;
}

void selecting_clicked(int x, int y)
{
  int sel = panel->win_coord_to_piece(x, y);
  if (sel >= 0) {
    change_status(IDLE);
    pipewrite(sel);
  }
}

void placing_clicked(int x, int y)
{
  glm::vec4 obj_coords = win_to_world(x, y);

  if (obj_coords.y < 0.0f and obj_coords.y > -0.1f) {
    int square = Board::coords_to_square(obj_coords.x, obj_coords.z);
    if (board.is_empty(square)) {
      change_status(IDLE);
      board.hover_piece(nullptr, 0);
      pipewrite(square);
    }
  }
}

void placing_hover(int x, int y)
{
  glm::vec4 obj_coords = win_to_world(x, y);

  if (obj_coords.y < 0.0f and obj_coords.y > -0.1f) {
    int square = Board::coords_to_square(obj_coords.x, obj_coords.z);
    if (board.is_empty(square))
      board.hover_piece(Piece::pieces[hover_piece], square);
    else
      board.hover_piece(nullptr, 0);
  } else {
    board.hover_piece(nullptr, 0);
  }
}


/***************  RUNS IN GAME THREAD *****************/

void do_place(int position)
{
  board.place_piece(Piece::pieces[hover_piece], position);
  panel->place_piece(hover_piece);
  hover_piece = 0;
  pipewrite(BASTL_ACK);
}

void do_highlight(int piece)
{
  player = !player;
  change_status(HIGHLIGHTING);
  hover_piece = piece;
  panel->highlight(piece);
}

