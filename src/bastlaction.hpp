#ifndef BASTLACTION_H_9CYQ25ZW
#define BASTLACTION_H_9CYQ25ZW

enum BastlStatus{
  IDLE,
  SELECTING,
  HIGHLIGHTING,
  PLACING,
  WON,
  TIE,
  CONNERROR
};

const char BASTL_ACK = 127;

extern BastlStatus bastl_status;
extern bool player;
extern int localplayer;

void change_status(BastlStatus status);
void pipewrite(char res);

void selecting_clicked(int x, int y);
void placing_clicked(int x, int y);
void placing_hover(int x, int y);

// Below runs in game thread
void do_place(int position);
void do_highlight(int piece);

#endif /* end of include guard: BASTLACTION_H_9CYQ25ZW */
