#ifndef BOARD_H_B4UWPX7R
#define BOARD_H_B4UWPX7R

#include "PV112.h"

struct Piece;

class Board
{
  private:
    PV112::PV112Geometry board_geom;
    float rotation = 0.0f;
    float win_anim = 0.0f;
    Piece *pieces[16];
    Piece *hover = nullptr;
    Piece *piece_to_place = nullptr;
    int pos_to_place;

  public:
    Board();
    Board(int pos_l, int norm_l, int tex_l);
    virtual ~Board();
    void reinit();
    void draw(float frtime);
    void set_rotation(float r);
    void place_piece(Piece *p, int position);
    void hover_piece(Piece *p, int position);
    bool is_empty(int position);

    static int coords_to_square(float x, float z);

    glm::mat4 projection_matrix,
              view_matrix,
              model_matrix,
              PVM_matrix;
    int active_spotlights = 0;
    int spotlit_pieces[4];
};


#endif /* end of include guard: BOARD_H_B4UWPX7R */
