#ifndef GUI_H_TAHK0D1Y
#define GUI_H_TAHK0D1Y

#include <glm/glm.hpp>
#include "bastlaction.hpp"

class GUI
{
  private:
    const float positions[33] = {
      0.0f, 0.0f, 0.0f,
      1.0f, 0.0f, 0.0f,
      0.0f, -0.2f, 0.0f,
      1.0f, -0.2f, 0.0f,
      // Volume triangle
      0.0f, 0.0f, 0.0f,
      1.0f, 0.0f, 0.0f,
      1.0f, 0.33f, 0.0f,
      // Spinner
      0.0f, 1.0f, 0.0f,
      -1.0f, 0.0f, 0.0f,
      1.0f, 0.0f, 0.0f,
      0.0f, -1.0f, 0.0f,
    };
    float texture_coords[22] = {
      0.0f, 0.125f,
      1.0f, 0.125f,
      0.0f, 0.0f,
      1.0f, 0.0f,
      // Volume triangle
      0.0f, 0.0f,
      1.0f, 0.0f,
      1.0f, 1.0f,
      // Spinner
      1.0f, 1.0f,
      0.0f, 1.0f,
      1.0f, 0.0f,
      0.0f, 0.0f,
    };
    unsigned vbo_positions;
    unsigned vbo_texture_coords;
    unsigned vao;
    unsigned diffuse_texture;
    unsigned spinner_texture;
    int tex_coord_location;
    bool needchange = false;
    float idled = 0.0f;

  public:
    GUI(int position_loc, int tex_coord_loc);
    virtual ~GUI();

    void draw();
    void bastl_status_changed(BastlStatus status);
};

extern GUI *gui;

#endif /* end of include guard: GUI_H_TAHK0D1Y */
