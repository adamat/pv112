#ifndef BASTLAPI_H_JQ8RCQ2T
#define BASTLAPI_H_JQ8RCQ2T

enum WinType {
  NONE,
  HORIZONTAL,
  VERTICAL,
  DIAGONAL
};

char piperead();
void bastl_select_piece();
void bastl_select_position();
void bastl_highlight_piece(char piece);
void bastl_place_piece(char position);
void bastl_win(WinType type, int position);

#endif /* end of include guard: BASTLAPI_H_JQ8RCQ2T */
