#ifndef UTILS_H_B3YPKSUO
#define UTILS_H_B3YPKSUO

#include <thread>
#include <GL/glxew.h>
#include <glm/glm.hpp>
#include "board.hpp"
#include "panel.hpp"
#include "gui.hpp"
#include "game_controller.hpp"

// Program, uniforms, global objects etc.
extern int win_width;
extern int win_height;
extern float app_time_s;

extern GLuint program;
extern GLint model_matrix_loc;
extern GLint PVM_matrix_loc;
extern GLint normal_matrix_loc;
extern GLint general_alpha_loc;
extern GLint light_position_loc;
extern GLint light_diffuse_color_loc;
extern GLint light_ambient_color_loc;
extern GLint light_specular_color_loc;
extern GLint spotlight_position_loc;
extern GLint n_spotlights_loc;
extern GLint eye_position_loc;
extern GLint texture_loc;
extern GLint normal_texture_loc;
extern GLint piece_number_loc;

extern glm::vec3 player_color[2];

extern Board board;
extern Panel *panel;
extern GUI *gui;
extern PV112::PV112Camera camera;
extern std::thread game_thread;

/***********
 *    ^ +y
 *    |
 *    |
 *    +------> +x
 *   /
 *  /
 * L +z
 */

bool process_arguments(int argc, char **argv, PlayersLoc *players_loc,
    int *in_fd, int *out_fd);
void print_usage();
void init();
void init_shading_stuff(int &position_loc, int &normal_loc, int &tex_coord_loc);

void key_pressed(unsigned char key, int mouseX, int mouseY);

void reshape(int width, int height);
void render();

void mouse_button_changed(int button, int state, int x, int y);
void mouse_moved(int x, int y);
void mouse_hover(int x, int y);

void simple_debug_callback(GLenum source, GLenum type, GLuint id, GLenum
    severity, GLsizei length, const char* message, const void* userParam);

void timer(int);

#endif /* end of include guard: UTILS_H_B3YPKSUO */
