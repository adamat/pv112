#ifndef PIECE_H_JXEQWCZZ
#define PIECE_H_JXEQWCZZ

#include <glm/glm.hpp>
#include "PV112.h"

class Piece
{
  public:
    GLint number;
    PV112::PV112Geometry *geom;
    int board_x;
    int board_y;

    Piece(unsigned geom_index, unsigned tex_index);
    glm::vec3 get_translation_vector();

    static PV112::PV112Geometry geometries[8];
    static Piece *pieces[16];
    static void load_pieces(int pos_l, int norm_l, int tex_l);
};

/***** Pieces *****
 *    8  4  2  1
 *   --------------
 * 0: L  [] .  [ ]
 * 1: D  O  |  [o]
 *    ^  ^  ^   `---- Solid top/hollow top
 *    |  |   `------- Short/tall
 *    |   `---------- Square/round
 *     `------------- Light/dark
 */

#endif /* end of include guard: PIECE_H_JXEQWCZZ */
