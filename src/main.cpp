#include <iostream>
#include <unistd.h>
#include <libgen.h>
#include "PV112.h"

#define _USE_MATH_DEFINES
#include <math.h>

//#define GLEW_STATIC
#include <GL/glew.h>
#include <IL/il.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <thread>

#include "utils.hpp"
#include "game_controller.hpp"

int win_width = 1000;
int win_height = 600;

extern int pipe_write_fd;
extern int pipe_read_fd;

std::thread game_thread;

int main(int argc, char **argv)
{
  if(chdir(dirname(argv[0]))){
    perror("Cannot change directory");
    return errno;
  }

  PlayersLoc pl = BOTH_LOCAL;
  int in_fd, out_fd;
  if (not process_arguments(argc, argv, &pl, &in_fd, &out_fd)) {
    print_usage();
    return 1;
  }
  // Initialize GLUT
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE);
  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

  // Set OpenGL Context parameters
  glutInitContextVersion(3, 3);
  glutInitContextProfile(GLUT_CORE_PROFILE);
  glutInitContextFlags(GLUT_DEBUG);

  // Initialize window
  glutInitWindowSize(win_width, win_height);
  glutCreateWindow("Quarto");

  // Initialize GLEW
  glewExperimental = GL_TRUE;
  glewInit();

  glEnable(GL_MULTISAMPLE);

  // Initialize DevIL library
  ilInit();

  // Set the debug callback
  PV112::SetDebugCallback(simple_debug_callback);

  Panel the_panel;
  panel = &the_panel;

  init();

  // Register callbacks
  glutDisplayFunc(render);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(key_pressed);
  glutTimerFunc(20, timer, 0);
  glutMouseFunc(mouse_button_changed);
  glutMotionFunc(mouse_moved);
  glutPassiveMotionFunc(mouse_hover);

  int pipefd[2];
  pipe(pipefd);
  pipe_write_fd = pipefd[1];
  pipe_read_fd = pipefd[0];

  game_thread = std::thread(game_controller, pl, in_fd, out_fd);

  // Run the main loop
  glutMainLoop();

  close(pipe_write_fd);
  game_thread.join();

  return 0;
}
