#ifndef NET_H_ICIN4KO9
#define NET_H_ICIN4KO9

#ifndef PORT_DEFAULT
#define PORT_DEFAULT 9470
#endif

int listen_and_accept(int port);
int connect_to(const char *hname, int port);

#endif /* end of include guard: NET_H_ICIN4KO9 */
