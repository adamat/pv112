#include <algorithm>
#include <iostream>
#include "sound.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "panel.hpp"
#include "utils.hpp"
#include "piece.hpp"
#include "bastlaction.hpp"

using std::min;
using std::max;

Panel::Panel() :
  anim_progress {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  model_matrix (
    glm::rotate(glm::mat4(1.0), 0.7f, glm::vec3(1.0f, 0.0f, 0.0f))
    * glm::rotate(glm::mat4(1.0), 0.2f, glm::vec3(0.0f, 1.0f, 0.0f))
    * glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 0.0f, -1.0f)))
{
  highlight_model_matrix =
    glm::translate(glm::mat4(1.0), glm::vec3(-8.2f, 1.5f, 6.0f))
    * glm::scale(glm::mat4(1.0), glm::vec3(1.3))
    * glm::rotate(glm::mat4(1.0), 0.8f, glm::vec3(1.0f, 0.0f, 0.0f))
    * glm::rotate(glm::mat4(1.0), 0.78f, glm::vec3(0.0f, 1.0f, 0.0f))
    * glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 0.0f, -1.0f));

    diffuse_texture = PV112::CreateAndLoadTexture("panel_diffuse.tga");
    normal_texture = PV112::CreateAndLoadTexture("panel_normal.tga");
    glBindTexture(GL_TEXTURE_2D, diffuse_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, normal_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Panel::reinit()
{
  memset(anim_progress, 0, 16 * sizeof(float));
  to_be_placed = -1;
  to_be_highlit = -1;
  highlighting = -1;
  hovered = -1;
}

void Panel::update_anims(float frtime)
{
  for (int i = 0; i < 16; i++) {
    if (anim_progress[i] >= 0.0f and anim_progress[i] <= 1.0f) {
      if (i == hovered) {
        anim_progress[i] =
          min(1.0f, anim_progress[i] + frtime/A_HOVER_DURATION);
      } else {
        anim_progress[i] =
          max(0.0f, anim_progress[i] - frtime/A_HOVER_DURATION);
      }
    } else if (anim_progress[i] < 0.0f and anim_progress[i] >= -1.0) {
        anim_progress[i] =
          max(-1.0f, anim_progress[i] - frtime/A_HLIGHT_DURATION);
    }
  }
}

glm::mat4 Panel::get_hlight_matrix(float progress)
{
  //smoothstep
  float x = 3.0f*progress*progress - 2.0f*progress*progress*progress;
  glm::mat4 res = x * highlight_model_matrix + (1.0f - x) * saved_model_matrix;
  return res;
}

void Panel::draw(float frtime)
{
  /*** Update ***/
  if (to_be_placed > -1) {
    anim_progress[to_be_placed] = PIECE_NOTAVAIL;
    to_be_placed = -1;
  }

  if (to_be_highlit > -1) {
    saved_model_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(
          (to_be_highlit / 4) * 2.6 - 5.0,
          (to_be_highlit % 4) * -6.5 + (to_be_highlit % 4 == 0 ? 6.8:8.3),
          8.0f))
      * glm::rotate(glm::mat4(1.0), 0.25f * anim_progress[to_be_highlit],
          glm::vec3(1.0f, 0.0f, 0.0f)) * model_matrix;
    anim_progress[to_be_highlit] = -0.0001;
    highlighting = to_be_highlit;
    to_be_highlit = -1;
  }

  update_anims(frtime);

  /*** Render ***/
  glUniform1i(n_spotlights_loc, 0);
  glUniform4f(light_position_loc, 0.0f, -0.3f, 3.0f, 0.0f);
  glUniform4f(1 + light_position_loc, 0.f, 0.5f, 2.0f, 0.0f);
  if (bastl_status == SELECTING) {
    glm::vec3 col = .25f * (1.f + sinf(5 * app_time_s)) * player_color[player];
    glUniform3fv(1 + light_diffuse_color_loc, 1, glm::value_ptr(col));
    glUniform3fv(1 + light_specular_color_loc, 1, glm::value_ptr(col));
  } else {
    glUniform3f(1 + light_diffuse_color_loc, .0f, .0f, .0f);
    glUniform3f(1 + light_specular_color_loc, 0.0f, 0.0f, 0.0f);
  }

  view_matrix = glm::lookAt(glm::vec3(0, 0, 8), glm::vec3(0.0f, 0.0f,
      0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
  float fww = win_width;
  float fwh = win_height;
  glViewport(fww - (panelratio * fwh), 0, panelratio * fwh, fwh);
  projection_matrix = glm::ortho(-10.0f, 5.0f, -7.5f / panelratio,
      7.5f / panelratio, -10.0f, 100.0f);
  glm::mat4 PVM_matrix;
  glm::mat3 normal_matrix;

  glUniform1f(general_alpha_loc, 1.0f);
  glUniform3f(eye_position_loc, 0, 0, 8);

  // Draw panel
  glUniform1f(general_alpha_loc, 1.0f);
  glBindVertexArray(geom.VAO);
  glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(glm::mat4(1.0)));
  PVM_matrix = projection_matrix * view_matrix;
  glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(PVM_matrix));
  normal_matrix = glm::mat3(1.0);
  glUniformMatrix3fv(normal_matrix_loc, 1, GL_FALSE,
      glm::value_ptr(normal_matrix));

  glUniform1i(texture_loc, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, diffuse_texture);
  glUniform1i(normal_texture_loc, 1);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, normal_texture);
  glUniform1i(piece_number_loc, -2);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  PV112::DrawGeometry(geom);
  glDisable(GL_BLEND);
  //
  // Draw pieces

  for(int i = 0; i < 16; i++){
    if (anim_progress[i] == PIECE_NOTAVAIL)
      continue;

    Piece *p = Piece::pieces[i];
    glBindVertexArray(p->geom->VAO);
    glm::mat4 piece_model_matrix;
    if (i == highlighting) {
      piece_model_matrix = get_hlight_matrix(-anim_progress[i]);
      if (bastl_status == HIGHLIGHTING and anim_progress[i] == -1.0f) {
        bastl_status = IDLE;
        pipewrite(BASTL_ACK);
      }
    } else {
      piece_model_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(
            (i / 4) * 2.6 - 5.0,
            (i % 4) * -6.5 + (i % 4 == 0 ? 6.8:8.3), 1.0f))
        * glm::rotate(glm::mat4(1.0), 0.25f * anim_progress[i],
            glm::vec3(1.0f, 0.0f, 0.0f)) * model_matrix;
    }

    PVM_matrix = projection_matrix * view_matrix * piece_model_matrix;
    normal_matrix = glm::inverse(glm::transpose(glm::mat3(piece_model_matrix)));

    glUniformMatrix4fv(model_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(piece_model_matrix));
    glUniformMatrix4fv(PVM_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(PVM_matrix));
    glUniformMatrix3fv(normal_matrix_loc, 1, GL_FALSE,
        glm::value_ptr(normal_matrix));

    glUniform1i(piece_number_loc, p->number);

    PV112::DrawGeometry(*(p->geom));
  }
  glBindVertexArray(0);
}

void Panel::place_piece(int piece)
{
  to_be_placed = piece;
}

void Panel::hover(int x, int y)
{
  hovered = win_coord_to_piece(x, y);
}

void Panel::highlight(int piece)
{
  to_be_highlit = piece;
  sound_mgr->play_sound(SND_SELECT);
}

int Panel::win_coord_to_piece(int x, int y)
{
  if (x < win_width - panelratio * win_height)
    return -1;
  GLfloat depth;
  glReadPixels(x, win_height - y - 1, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT,
      &depth);
  glm::vec4 viewport(win_width - (panelratio * win_height), 0,
      panelratio * win_height, win_height);
  glm::vec3 win_coords(x, win_height - y - 1, depth);
  glm::vec4 obj_coords(glm::unProject(win_coords, view_matrix,
        projection_matrix, viewport), depth);
  if (obj_coords.x < -5.5f or obj_coords.z < 0.0f)
    return -1;
  int piece = int((obj_coords.x + 5.4f) / 2.6f) * 4;
  if (obj_coords.y < 6.8f)
    piece += int((obj_coords.y - 14.3f) / -6.5);
  return piece;
}

Panel::~Panel()
{
}

