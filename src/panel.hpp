#ifndef PANEL_H_4DFT72YT
#define PANEL_H_4DFT72YT

#include <GL/glew.h>
#include "PV112.h"

class Panel
{
  private:
    /* 0.0 - 1.0    = hovering anim
     * 0.0 - (-1.0) = highlighting anim
     * -10.0        = not available */
    const float PIECE_NOTAVAIL = -100.0f;
    const float A_HOVER_DURATION = 0.2f; //s
    const float A_HLIGHT_DURATION = 0.5f; //s
    float anim_progress[16];

    const float panelratio = 0.6f; //Update the obj file and the board persp.
    int to_be_placed = -1;
    int to_be_highlit = -1;
    int highlighting = -1;
    int hovered = -1;

    const glm::mat4 model_matrix;
    glm::mat4 saved_model_matrix;
    glm::mat4 highlight_model_matrix;
    glm::mat4 view_matrix;
    glm::mat4 projection_matrix;
    GLuint diffuse_texture;
    GLuint normal_texture;

    void update_anims(float frtime);
    glm::mat4 get_hlight_matrix(float progress);

  public:
    Panel();
    virtual ~Panel ();
    void reinit();
    void draw(float frtime);
    void place_piece(int piece);
    void hover(int x, int y);
    void highlight(int piece);
    int win_coord_to_piece(int x, int y);

    PV112::PV112Geometry geom;
};

#endif /* end of include guard: PANEL_H_4DFT72YT */
